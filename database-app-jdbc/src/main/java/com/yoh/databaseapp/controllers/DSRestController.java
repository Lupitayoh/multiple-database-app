package com.yoh.databaseapp.controllers;

import com.yoh.databaseapp.repositories.ProfileServiceDao;
import com.yoh.databaseapp.repositories.TaskServiceDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DSRestController {

    @Autowired
    ProfileServiceDao profileServiceDao;

    @Autowired
    TaskServiceDao taskServiceDao;


    @RequestMapping(value = "/userscount")
    public String userCount_from_ProfileService(){
        return "Count from users table is: " + profileServiceDao.getCount_from_users();
    }

    @RequestMapping(value = "/taskcount")
    public String taskCount_from_TaskService(){
        return "Count from tasks table is: " + taskServiceDao.getCount_from_tasks();
    }
}
