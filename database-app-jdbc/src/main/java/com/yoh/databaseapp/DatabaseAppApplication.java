/**
 * Project from the video tutorial "How to create Multiple Data Source in single Spring Boot application"
 * https://www.youtube.com/watch?v=exuGufC9BS8
 */
package com.yoh.databaseapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DatabaseAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(DatabaseAppApplication.class, args);
	}
}


