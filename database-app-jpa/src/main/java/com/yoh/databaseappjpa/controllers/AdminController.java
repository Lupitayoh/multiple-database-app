package com.yoh.databaseappjpa.controllers;

import com.yoh.databaseappjpa.models.seconds.Admin;
import com.yoh.databaseappjpa.services.seconds.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("admin")
public class AdminController {

    @Autowired
    private AdminService adminService;

    @RequestMapping(method = RequestMethod.GET)
    public Admin getAdmin(String name, String pass){

        return adminService.getAdmin(name, pass);
    }

    @RequestMapping(method = RequestMethod.POST)
    @ResponseBody
    public Admin saveAdmin(@RequestParam("name") String name, @RequestParam("pass") String pass){
        System.out.println("name: " + name);
        return adminService.saveAdmin(name, pass);
    }
}
