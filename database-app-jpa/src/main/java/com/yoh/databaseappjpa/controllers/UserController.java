package com.yoh.databaseappjpa.controllers;

import com.yoh.databaseappjpa.models.firsts.User;
import com.yoh.databaseappjpa.services.firsts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("user")
public class UserController {

    @Autowired
    private UserService userService;

    @RequestMapping(method = RequestMethod.GET)
    public User getUser(String name, String pass){
        return userService.getUser(name, pass);
    }

    @RequestMapping(method = RequestMethod.POST)
    @ResponseBody
    public User saveUser(@RequestParam("name") String name, @RequestParam("pass") String pass){
        return userService.saveUser(name, pass);
    }
}
