package com.yoh.databaseappjpa.repository.firsts;

import com.yoh.databaseappjpa.models.firsts.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends CrudRepository<User, Long> {

    User findByNameAndPass(String name, String pass);
}
