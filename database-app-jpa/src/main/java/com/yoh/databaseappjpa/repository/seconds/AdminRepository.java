package com.yoh.databaseappjpa.repository.seconds;

import com.yoh.databaseappjpa.models.seconds.Admin;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AdminRepository extends CrudRepository<Admin, Long> {

    Admin findByNameAndPass(String name, String pass);
}
