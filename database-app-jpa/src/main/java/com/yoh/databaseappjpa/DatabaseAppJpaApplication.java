package com.yoh.databaseappjpa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceTransactionManagerAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication(
		scanBasePackages = {"com.yoh.databaseappjpa"},
		exclude = {
				HibernateJpaAutoConfiguration.class,
				DataSourceAutoConfiguration.class,
				DataSourceTransactionManagerAutoConfiguration.class
		}
)
@EnableTransactionManagement
public class DatabaseAppJpaApplication {

	public static void main(String[] args) {
		SpringApplication.run(DatabaseAppJpaApplication.class, args);
	}
}
