package com.yoh.databaseappjpa.services.firsts;

import com.yoh.databaseappjpa.models.firsts.User;
import com.yoh.databaseappjpa.repository.firsts.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    @Transactional("firstsTransactionManager")
    public User saveUser(String name, String pass){
        User user = new User();
        user.setName(name);
        user.setPass(pass);
        user.setState("NEW");
        userRepository.save(user);
        return user;
    }

    public User getUser(String name, String pass){
        return userRepository.findByNameAndPass(name, pass);
    }
}
