package com.yoh.databaseappjpa.services.seconds;

import com.yoh.databaseappjpa.models.seconds.Admin;
import com.yoh.databaseappjpa.repository.seconds.AdminRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class AdminService {

    @Autowired
    private AdminRepository adminRepository;

    @Transactional("secondsTransactionManager")
    public Admin saveAdmin(String name, String pass){
        Admin admin = new Admin();
        admin.setName(name);
        admin.setPass(pass);
        admin.setState("NEW");
        System.out.println(admin);
        adminRepository.save(admin);
        return admin;
    }

    public Admin getAdmin(String name, String pass){
        return adminRepository.findByNameAndPass(name, pass);
    }
}
