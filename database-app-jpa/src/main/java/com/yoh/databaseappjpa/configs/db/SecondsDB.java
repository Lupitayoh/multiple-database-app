package com.yoh.databaseappjpa.configs.db;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;

import javax.sql.DataSource;

@Configuration
@EnableJpaRepositories(
        basePackages = "com.yoh.databaseappjpa.repository.seconds",
        entityManagerFactoryRef = "secondsEntityManagerFactory",
        transactionManagerRef = "secondsTransactionManager"
)
public class SecondsDB {
    @Value("${datasource.seconds.driver}")
    private String driver;
    @Value("${datasource.seconds.url}")
    private String url;
    @Value("${datasource.seconds.username}")
    private String username;
    @Value("${datasource.seconds.password}")
    private String pass;

    @Bean
    public DataSource secondsDataSource(){
        HikariConfig config = new HikariConfig();
        config.setDriverClassName(driver);
        config.setJdbcUrl(url);
        config.setUsername(username);
        config.setPassword(pass);
        return new HikariDataSource(config);
    }

    @Bean
    public PlatformTransactionManager secondsTransactionManager(){
        return new JpaTransactionManager(secondsEntityManagerFactory().getObject());
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean secondsEntityManagerFactory(){
        HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        LocalContainerEntityManagerFactoryBean factory = new LocalContainerEntityManagerFactoryBean();
        factory.setJpaVendorAdapter(vendorAdapter);
        factory.setDataSource(secondsDataSource());
        factory.setPackagesToScan("com.yoh.databaseappjpa.models");
        return factory;
    }
}
