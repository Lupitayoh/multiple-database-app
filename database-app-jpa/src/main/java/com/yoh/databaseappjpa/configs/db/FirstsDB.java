package com.yoh.databaseappjpa.configs.db;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;

import javax.sql.DataSource;

@Configuration
@EnableJpaRepositories(
        basePackages = "com.yoh.databaseappjpa.repository.firsts",
        entityManagerFactoryRef = "firstsEntityManagerFactory",
        transactionManagerRef = "firstsTransactionManager"
)
public class FirstsDB {

    @Value("${datasource.firsts.driver}")
    private String driver;
    @Value("${datasource.firsts.url}")
    private String url;
    @Value("${datasource.firsts.username}")
    private String username;
    @Value("${datasource.firsts.password}")
    private String pass;

    @Bean
    public DataSource firstsDataSource(){
        HikariConfig config = new HikariConfig();
        config.setDriverClassName(driver);
        config.setJdbcUrl(url);
        config.setUsername(username);
        config.setPassword(pass);
        return new HikariDataSource(config);
    }

    @Bean
    public PlatformTransactionManager firstsTransactionManager(){
        return new JpaTransactionManager(firstsEntityManagerFactory().getObject());
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean firstsEntityManagerFactory(){
        HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        LocalContainerEntityManagerFactoryBean factory = new LocalContainerEntityManagerFactoryBean();
        factory.setJpaVendorAdapter(vendorAdapter);
        factory.setDataSource(firstsDataSource());
        factory.setPackagesToScan("com.yoh.databaseappjpa.models");
        return factory;
    }
}
